
let debounce = function(c, d){
  let execTimer = null;
  return function() {
      if (execTimer) clearTimeout(execTimer);
      execTimer = setTimeout(function(){
          execTimer = null;
          c();
      }, d);
  }
};

let wait = function(ms) {
  return new Promise((res, rej)=>{
    setTimeout(function() {
      res();
    }, ms);
  });
};


let formattedBytes = function(bytes) {
  let b = bytes;
  let t = 1000;
  let i = 0;
  let e = ['', 'K', 'M', 'G', 'T'];
  while (b > 1000) {
    b/=t;
    i++;
    if (i === e.length-1) break;
  }
  return Math.floor(b*100+0.5)/100 + ' ' + e[i]+'B';
}

function appendLeadingZeroes(n){
  if(n <= 9) { return "0" + n; } return "" + n
}

function formatDateSystem(current_datetime) {
  return current_datetime.getFullYear() + "-" + appendLeadingZeroes(current_datetime.getMonth() + 1) + "-" + appendLeadingZeroes(current_datetime.getDate()) + " " + appendLeadingZeroes(current_datetime.getHours()) + ":" + appendLeadingZeroes(current_datetime.getMinutes()) + ":" + appendLeadingZeroes(current_datetime.getSeconds());
}


function formatDate(current_datetime, lang /* TODO date format localisation */) {
  return appendLeadingZeroes(current_datetime.getDate()) + "." + appendLeadingZeroes(current_datetime.getMonth() + 1) + "." + current_datetime.getFullYear()
  + " "
  + appendLeadingZeroes(current_datetime.getHours()) + ":" + appendLeadingZeroes(current_datetime.getMinutes()) + ":" + appendLeadingZeroes(current_datetime.getSeconds());
}

export {debounce, wait, formattedBytes, appendLeadingZeroes, formatDateSystem, formatDate};