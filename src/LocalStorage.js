//import { openDB, deleteDB, wrap, unwrap } from 'idb';
import dbPromise from './LocalDatabase';
import moment from 'moment';

moment.locale('de');

export default (function() {
  'use strict';

  return {


    getDB() {
      return dbPromise;
    },

    async getTexts() {
      const db = await dbPromise;
      return await db.getAll('texts');
    },


    async saveText(text, autoUpdateTime) {
      if (!text.dirty) return text;

      if (typeof text.key === 'undefined') {
        throw 'key undefined';
      }
      if (typeof autoUpdateTime === 'undefined') {
        autoUpdateTime = true;
      }
      if (autoUpdateTime) {
        // https://momentjs.com/
        text.updated_at = moment().format("YYYY-MM-DD HH:mm:ss");
      }
      delete text.dirty;
      const db = await dbPromise;
      await db.put('texts', text);
      text.dirty = false;
      return text;
    },

    async getText(keys) {
      const db = await dbPromise;
      let text = await db.get('texts', keys);
      if (typeof text === 'undefined') {
        throw 'text not found';
      }
      return text
    },






  };
})();








