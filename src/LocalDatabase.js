import { openDB, deleteDB, wrap, unwrap } from 'idb';

export default (function() {
  'use strict';

  //check for support
  if (!('indexedDB' in window)) {
    console.error('This browser doesn\'t support IndexedDB');
    return;
  }

  // https://github.com/jakearchibald/idb
  // https://developers.google.com/web/ilt/pwa/working-with-indexeddb?hl=de

  var dbPromise = openDB('journal', 2, {
    upgrade(db, oldVersion, newVersion, transaction) {
      console.log('upgrade', oldVersion, newVersion);
      let store;
      switch (oldVersion) {
        case 0: // OLD
        store = db.createObjectStore('texts', {keyPath: 'key', autoIncrement : false});
        case 1: // OLD
        store = db.createObjectStore('settings', {});
        //store.createIndex('id', 'id');
      }
  }});

  return dbPromise;
})();








