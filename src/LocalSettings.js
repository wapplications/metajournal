import dbPromise from "./LocalDatabase";
//import ld from "lowdash";

const _ = require('lodash');

let settings = {};

function init() {
  return dbPromise.then(db=>{
    return db.getAllKeys('settings').then(keys=>{
      let ps = [];
      keys.forEach(k => {
        ps.push(db.get('settings', k).then((v)=>{
          settings[k]=v;
        }));
      });
      return Promise.all(ps).then(()=>{
        console.log('settings loaded');
      });
    });
  });
}

function get(n, d) {
  let v = _.get(settings, n);
  if (typeof v === 'undefined') return d;
  return v;
}

function set(n, v) {
  _.set(settings, n, v);
  return dbPromise.then(db=>db.put('settings', v, n));
}

function getAll() {
  return settings;
}

export default {init, get, set, getAll};
