import { saveAs } from 'file-saver';
import moment from 'moment';

/**
 * https://github.com/eligrey/FileSaver.js
 * https://developers.google.com/web/updates/2011/08/Saving-generated-files-on-the-client-side
 */

export function isSupported() {
  try {
    var isFileSaverSupported = !!new Blob;
    return true;
  } catch (e) {}
  return false;
}






export function importer() {

  let inp = document.createElement('input');
  inp.setAttribute('type', 'file');
  inp.setAttribute('accept', 'application/json');
  inp.style.display = 'none';
  inp.addEventListener("change", inpChanged, false);
  document.querySelector('body').appendChild(inp);

  let resolve = null, reject = null;

  function inpChanged() { // promisify or asyncify
    let self = this;
    const selectedFile = inp.files[0];
    if (selectedFile) {
      const reader = new FileReader();
      reader.onload = function(progress) {
        if (progress.loaded === progress.total) {
          try {
            let importData = JSON.parse(reader.result);
            resolve(importData);
          } catch (e) {
            reject('data broken');
          }
        }
      };
      reader.onerror = function(e) {
        reject(e);
      }
      reader.readAsText(selectedFile);
    } else {
      resolve(false);
    }
    // TODO andere fehler abfangen?
  }

  return function() {
    return new Promise((res, rej)=>{
      resolve = res;
      reject = rej;
      inp.value = '';
      // https://developer.mozilla.org/en-US/docs/Web/API/File/Using_files_from_web_applications
      inp.click();
    });
  }
};

export function exporter(defaultFilename, getExportData) {
  return async function() {
    const exportData = await getExportData();
    if (!exportData) return;
    const exportJSON = JSON.stringify(exportData, null, '  ');
    const exportBlob = new Blob([exportJSON], {type: "application/json"});
    saveAs(exportBlob, defaultFilename);
  }
};






export let StorageDataManager = {
  name: 'storage-data-manager',
  template: `<div>
  <v-ons-card>
  <div class="title">
    Daten exportieren
  </div>
  <div class="content">
    <v-ons-list>
      <v-ons-list-header>Daten auswählen</v-ons-list-header>
      <v-ons-list-item v-for="(s,k) in exportStores" :key="k">
        <label><input type="checkbox" v-model="s.selected" /> {{ k }}</label>
      </v-ons-list-item>
    </v-ons-list>
    <v-ons-button @click="onExportClick" >export</v-ons-button>
  </div>
</v-ons-card>
<v-ons-card>
  <div class="title">
    Daten importieren
  </div>
  <div class="content">
    <v-ons-button v-if="importStores === null" @click="onImportClick" >import</v-ons-button>
    <template v-else>
      <progress v-if="progress !== null" style="width:100%;" max="100" :value="progress"> <span id="p">{{ Math.floor((progress)) }}</span>% </progress>
      <template v-else>
        <v-ons-list>
          <v-ons-list-header>Daten auswählen</v-ons-list-header>
          <v-ons-list-item v-for="(s,k) in importStores" :key="k">
            <label><input type="checkbox" v-model="s.selected" /> {{ k }}</label>
          </v-ons-list-item>
        </v-ons-list>
        <v-ons-button @click="onImportProceedClick" >Import ausführen</v-ons-button>
        <v-ons-button @click="onImportCancelClick" class="button--material--flat" >Import abbrechen</v-ons-button>
      </template>
    </template>
  </div>
</v-ons-card>

<v-ons-card>
<div class="title">
  Daten löschen
</div>
<div class="content">
  <v-ons-list>
    <v-ons-list-header>Daten auswählen</v-ons-list-header>
    <v-ons-list-item v-for="(s,k) in clearStores" :key="k">
      <label><input type="checkbox" v-model="s.selected" /> {{ k }}</label>
    </v-ons-list-item>
  </v-ons-list>
  <v-ons-button @click="onClearClick" >Löschen</v-ons-button>
</div>
</v-ons-card>

</div>`,
  data: function() {
    return {
      progress: null,
      exportStores: [],
      clearStores: [],
      importStores: null,
    }
  },

  props: ['db', 'fileNameBase'],


  unmount() {
    // TODO remove input element from importer
  },

  mounted(){

    this.doImport = importer();
    this.doExport = exporter(this.fileNameBase+'_'+moment().format('YYYY-MM-DD')+'.json', this.createExportData);
    
    
    this.getStoreKeys().then(keys => {
      this.exportStores = this.createStoresList(keys, true);
      this.clearStores = this.createStoresList(keys, false);
    });

    this.importProceed = null;
  },


  methods: {
    // TODO die ganze zweischrittiger import sache in InAndExport auslagern?

    createStoresList(keys, selected) {
      let stores = {};
      for (let i=0; i<keys.length; i++) {
        stores[keys[i]] = {selected: selected};
      };
      return stores;
    },

    async onClearClick() {
      let conf = await this.$ons.notification.confirm("Löschen?");
      if (conf !== 1) return;
      let db = await this.db;
      for (let storeName in this.clearStores) {
        if (!this.clearStores[storeName].selected) continue;
        await db.clear(storeName);
      }
    },

    onExportClick() {
      this.doExport();
    },

    async onImportClick() {
      let data = null;
      try {
        data = await this.doImport();
      } catch (e) {
        console.warn(e);
        data = null;
      }
      if (data) {
        let keys = Object.keys(data);
        this.importStores = this.createStoresList(keys);
        this.importProceed = {data: data};
        let promise = new Promise((res, rej)=>{
          this.importProceed.res = res;
          this.importProceed.rej = rej;
        });
        try {
          await promise;
          console.log('Import fertig');
        } catch (e) {
          console.warn(e);
        }
        this.importStores = null;
        this.importProceed = null;
        this.progress = null;
      }
    },

    async onImportProceedClick() {
      if (!this.importProceed) {
        return;
      }
      let data = {};
      let haveData = false;
      for (let storeName in this.importStores) {
        if (!this.importStores[storeName].selected) continue;
        if (typeof this.importProceed.data[storeName] === 'undefined') continue;
        data[storeName] = this.importProceed.data[storeName];
        haveData = true;
      }
      await this.importData(data);
      this.importProceed.res();
    },

    onImportCancelClick() {
      this.importProceed.rej();
    },

    async importData(data) {
      console.log('import',data);

      this.progress = 0;
      let totalItemsCount = 0;
      let progressCount = 0;
      let progress = () => {
        progressCount++;
        this.progress = progressCount*100/totalItemsCount;
        //console.log(this.progress);
      };
      for (let sn in data) {
        totalItemsCount += data[sn].length;
      }
      console.log(totalItemsCount);

      const db = await this.db;

      for (let sn in data) {
        try {
          db.clear(sn);
          for (let i = 0; i < data[sn].length; i++) {
            const element = data[sn][i];
            try {
              await db.put(sn, element);
            } catch (e) {
              console.warn(e);
            }
            progress();
          }
        } catch (e) {
          console.warn(e);
        }
      }

    },

    async getStoreKeys() {
      let db = await this.db;
      let stores = db.objectStoreNames;
      return stores;
    },

    async createExportData() {
      let db = await this.db;
      let data = {};
      let haveData = false;
      for (let storeName in this.exportStores) {
        if (!this.exportStores[storeName].selected) continue;
        data[storeName] = await db.getAll(storeName);
        haveData = true;
      }
      if (haveData) {
        console.log(data);
        return data;
      }
      return false;
    }

  }



};