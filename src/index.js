
// https://onsen.io/v2/guide/
// https://onsen.io/v2/guide/vue/#vue-js

// http://zavoloklom.github.io/material-design-iconic-font/icons.html

// JS import
import Vue from 'vue';
import VueOnsen from 'vue-onsenui'; // This imports 'onsenui', so no need to import it separately
import settings from './LocalSettings';

if ('serviceWorker' in navigator) {
  (async function(){
    // this creates 0.js or something like that
    const {Workbox} = await import('workbox-window');
    const wb = new Workbox('/service-worker.js');
    wb.addEventListener('waiting', (event) => {
      console.log(`A new service worker has installed, but it can't activate` +
          `until all tabs running the current version have fully unloaded.`);
    });
    wb.addEventListener('message', (event) => {
      if (event.data.type === 'CACHE_UPDATED') {
        const {updatedURL} = event.data.payload;
        console.log(`A newer version of ${updatedURL} is available!`);
      }
    });
    wb.addEventListener('installed', (event) => {
      //wb.skipWaiting();
      if (!event.isUpdate) {
        console.log('sw installed first time')
        
        // First-installed code goes here...
      } else {
        console.log('sw installed update')
      }
    });
    wb.addEventListener('activated', (event) => {
      //wb.clientsClaim();
      if (!event.isUpdate) {
        console.log('sw activated first time')
        // First-activated code goes here...
      } else {
        console.log('sw activated update RELOAD?')
        if (confirm('Reload?')) {
          window.location.reload();
        }
      }
    });
    wb.register();
  })()
}



Vue.use(VueOnsen); // VueOnsen set here as plugin to VUE. Done automatically if a call to window.Vue exists in the startup code.

Vue.component('main-app', require('./components/App.vue').default);

VueOnsen.platform.select('android')

window.log = console.log;
window.warn = console.warn;
window.error = console.error;


//wait for event: deviceready





async function initApp() {
  await settings.init();

  const app = new Vue({
    el: '#app',
    template:"<main-app></main-app>"
  
  });
}


initApp();

//console.log("Hallo Welt");
