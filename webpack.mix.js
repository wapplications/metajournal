const mix = require('laravel-mix');
const exec = require('child_process').exec;
const execSync = require('child_process').execSync;
const fs = require('fs');
const path = require('path');

const {GenerateSW} = require('workbox-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

require('dotenv').config(); // .env verfügbar machen!


let appName = process.env.APP_NAME;
let appShortName = appName;





// https://laravel-mix.com/docs/5.0/installation
// https://laravel-mix.com/extensions/copy-watched
// require('laravel-mix-copy-watched');
// https://laravel-mix.com/docs/5.0/extending-mix
if (!mix.inProduction()) {
   mix.sourceMaps();
}



mix.setPublicPath('public');

mix.js('src/index.js', 'js/');
mix.sass('src/styles/app.scss', 'css/');
mix.setPublicPath('public');


//mix.js('resources/js/service-worker.js', 'public/');


//mix.js('resources/js/landing.js', 'public/js')
//   .sass('resources/sass/landing.scss', 'public/css');


   //   
//mix.copyDirectory('node_modules/feather-icons/dist/icons', 'public/icons');
//mix.copy('node_modules/feather-icons/dist/feather-sprite.svg', 'public/icons/all.svg');







// if prod ??

let logoSizes = [48, 128, 144, 256, 512];

let mtime = function(fn) {
   if (fs.existsSync(fn)) {
      let stats = fs.statSync(fn);
      return stats.mtime;
   }
   return null;
};


//mix.copy('resources/graphics/map-illu.svg', 'public/images/map-illu.svg');
//mix.copy('resources/graphics/logo.svg', 'public/images/logo.svg');
//mix.copy('resources/graphics/logo-black-alpha-70.svg', 'public/images/logo-black-alpha-70.svg');
//mix.copy('resources/graphics/logo-black-alpha-10.svg', 'public/images/logo-black-alpha-10.svg');
//mix.copy('resources/graphics/logo-gold.svg', 'public/images/logo-gold.svg');
//mix.copy('graphics/app-logo.svg', 'public/graphics')

let copyPattern = [];

logoSizes.forEach(logoSize => {
   let filename = 'app-logo-'+logoSize+'x'+logoSize+'.png';
   let generatedFile = 'assets/images/'+filename;
   let inFile = 'assets/images/app-logo.svg';
   let inTime = mtime(inFile);
   let outTime = mtime(generatedFile);
   if (!outTime || inTime > outTime) {
      console.log(generatedFile);
      execSync('inkscape -f '+inFile+' -w '+logoSize+' -h '+logoSize+' -e '+generatedFile);
   }
   copyPattern.push({from: path.resolve(__dirname, generatedFile), to: path.resolve(__dirname, 'public/images/'+filename)});
});


//mix.copy(logos, 'public/images');


mix.then(function(s) {
   console.log('create logos & manifest.json');

   


   let manifest = {
      name: appName,
      short_name: appShortName,
      icons: [],
      "start_url": "/",
      "display": "standalone",
      "background_color": "#f5f5dc", // TODO
      "theme_color": "#1b5454",      // TODO
      "scope": "/",
   };
   logoSizes.forEach(logoSize => {
      manifest.icons.push({
         src: '/images/app-logo-'+logoSize+'x'+logoSize+'.png',
         sizes: logoSize+"x"+logoSize,
         type: "image/png"
      });
   });
   let fileContent = JSON.stringify(manifest, null, 2);
   fs.writeFile("public/manifest.json", fileContent, (e)=>{});
});

const addEntriesTransform = async (manifest) => {
   // da manche urls von webpack mit / vorangestellt kommen
   // und das beim precache probleme macht...
   manifest.forEach(m => {
      if (m.url.startsWith('//')) {
         m.url = m.url.replace(/^\/\//, '/');
      }
   });
   // startseite auch cachen
   manifest.push({url: '/', revision: mtime('assets/index.html') });
   manifest.push({url: '/manifest.json', revision: mtime('public/manifest.json') });
   return {manifest, warnings: []};
};


copyPattern.push({from: path.resolve(__dirname, 'assets', 'index.html'), to: 'index.html'});

mix.webpackConfig({
   //watch: false,
   devServer: {
      contentBase: path.join(__dirname, 'public'),
      compress: true,
      //port: 9000
    },
   plugins: [
      new GenerateSW({
         offlineGoogleAnalytics: false,
         maximumFileSizeToCacheInBytes: 20000000,
         clientsClaim: true,
         skipWaiting: true,
         // Bug: https://github.com/GoogleChrome/workbox/issues/2558
         //additionalManifestEntries: [
         //   {"url": '/index.html'}
         //]
         manifestTransforms: [addEntriesTransform]
      }),
      // https://webpack.js.org/plugins/copy-webpack-plugin/
      new CopyPlugin({
         patterns: copyPattern
      })
   ],
});
